"use strict";

const buttons = document.querySelectorAll(".btn-wrapper .btn");
const btns = ["enter", "s", "e", "o", "n", "l", "z"];
console.log(buttons);

const doInBlue = function (event) {
  const letter = event.key.toLowerCase();
  const buttonIndex = btns.indexOf(letter);
  if (buttonIndex !== -1) {
    buttons.forEach((button) => (button.style.backgroundColor = "black"));
    buttons[buttonIndex].style.backgroundColor = "blue";
  }
};

document.addEventListener("keydown", doInBlue);

const themeToggleBtn = document.querySelector(".theme-toggle");
const body = document.body;
const themeKey = "theme";

function toggleTheme() {
  if (body.classList.contains("blue")) {
    body.classList.remove("blue");
    localStorage.setItem(themeKey, "light");
  } else {
    body.classList.add("blue");
    localStorage.setItem(themeKey, "blue");
  }
}

const savedTheme = localStorage.getItem(themeKey);
if (savedTheme) {
  body.classList.add(savedTheme);
}

themeToggleBtn.addEventListener("click", toggleTheme);
